﻿using MovieFlix.Core.Contracts;

namespace MovieFlix.Core.Implementation
{
    public class LoggingService : ILoggingService
    {
        //TODO: Logging to be implemented by using MSMQ
        public void Info(string message)
        {

        }
        public void Warn(string message)
        {

        }
        public void Trace(string message)
        {

        }
        public void Error(string message)
        {

        }

        public void Fatal(string message)
        {

        }
    }
}
