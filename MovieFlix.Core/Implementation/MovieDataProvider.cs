﻿using System;
using System.Collections.Generic;
using MovieFlix.Core.Contracts;
using MoviesLibrary;
using System.Threading.Tasks;
using System.Linq;

namespace MovieFlix.Core.Implementation
{
    /// <summary>
    /// A Wrapper on top of MovieDataSource Because MovieDataSource does not have interfaces exposed, wrong implementation of MovieDataSource library.
    /// </summary>
    public class MovieDataProvider : IExternalDataSource
    {
        private MovieDataSource _mds;

        public MovieDataProvider(MovieDataSource mds)
        {
            _mds = mds;
        }

        public int Create(MovieData movie)
        {
            return _mds.Create(movie);
        }
        public IEnumerable<MovieData> GetAllData()
        {
            return _mds.GetAllData();

        }
        public MovieData GetDataById(int id)
        {
            return _mds.GetDataById(id);
        }

        public int Update(MovieData movie)
        {
            _mds.Update(movie);
            return movie.MovieId;
        }

    }
}
