﻿using MovieFlix.Core.Contracts;
using MoviesLibrary;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Linq.Dynamic;

namespace MovieFlix.Core.Implementation
{
    public class MovieRespository : IMovieRepository
    {
        private IExternalDataSource _movieDataSource;
        /// <summary>
        /// Initializes a new instance of the <see cref="MovieRespository"/> class.
        /// </summary>
        /// <param name="movieDataSource"></param>
        public MovieRespository(IExternalDataSource movieDataSource)
        {
            _movieDataSource = movieDataSource;
        }

        /// <summary>
        /// Loads the movies.
        /// </summary>
        /// <param name="movie">The movie.</param>
        /// <returns></returns>
        public IEnumerable<MovieData> LoadMovies(MovieData movie = null)
        {
            var predicate = PredicateBuilder.True<MovieData>();
            if (movie != null)
            {
                if (movie.MovieId > 0)
                {
                    predicate = predicate.And(p => p.MovieId == movie.MovieId);
                }

                if (!string.IsNullOrEmpty(movie.Title))
                {
                    predicate = predicate.And(p => p.Title.ToLower().Contains(movie.Title.ToLower()));
                }

                if (!string.IsNullOrEmpty(movie.Genre))
                {
                    predicate = predicate.And(p => p.Genre.ToLower().Contains(movie.Genre.ToLower()));
                }
                if (!string.IsNullOrEmpty(movie.Classification))
                {
                    predicate = predicate.And(p => p.Classification.ToLower().Contains(movie.Classification.ToLower()));
                }

                if (movie.Rating > 0)
                {
                    predicate=predicate.And(p => p.Rating == movie.Rating);
                }

                if (movie.ReleaseDate > 0)
                {
                    predicate = predicate.And(p => p.ReleaseDate == movie.ReleaseDate);
                }
                if (movie.Cast != null)
                {
                    if (movie.Cast.Count() > 0)
                    {
                        predicate = predicate.And(p => p.Cast.Any(movie.Cast.Contains));
                    }
                }
            }
            return  _movieDataSource.GetAllData().AsQueryable().Where(predicate);
        }

        /// <summary>
        /// Saves the movie.
        /// </summary>
        /// <param name="movie">The movie.</param>
        /// <exception cref="ArgumentNullException">Movie object can not be null</exception>
        public int SaveMovie(MovieData movie)
        {
            if (movie != null)
            {
                if (movie.MovieId > 0)
                {
                    _movieDataSource.Update(movie);
                    return movie.MovieId;
                }
                else
                {
                    return _movieDataSource.Create(movie);
                }
            }
            else
            {
                throw new ArgumentNullException("Movie Data can not be null.");
            }
        }
    }
}
