﻿using MoviesLibrary;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieFlix.Core.Contracts
{
    public interface IMovieRepository
    {
        /// <summary>
        /// Gets the movies.
        /// </summary>
        /// <returns></returns>
        IEnumerable<MovieData> LoadMovies(MovieData movie);

        /// <summary>
        /// Saves the movie.
        /// </summary>
        /// <param name="movie">The movie.</param>
        int SaveMovie(MovieData movie);
    }
}
