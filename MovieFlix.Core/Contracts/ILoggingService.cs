﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieFlix.Core.Contracts
{
    //TODO: Logging to be implemented by using MSMQ
    public interface ILoggingService
    {
        void Info(string message);

        void Warn(string message);

        void Trace(string message);

        void Error(string message);

        void Fatal(string message);

    }
}
