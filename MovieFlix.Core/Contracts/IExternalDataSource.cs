﻿using MoviesLibrary;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieFlix.Core.Contracts
{
    public interface IExternalDataSource
    {
        int Create(MovieData movie);

        IEnumerable<MovieData> GetAllData();

        MovieData GetDataById(int id);
        int Update(MovieData movie);

    }
}