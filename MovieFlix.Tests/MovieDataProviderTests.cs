﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MovieFlix.Core.Contracts;
using MoviesLibrary;
using System.Collections.Generic;
using MovieFlix.Core.Implementation;
using System.Linq;
namespace MovieFlix.Tests
{
    [TestClass]
    public class MovieDataProviderTests
    {
        public List<MovieData> _movieRepository;
        public Mock<MovieDataSource> _movieDataSource;
        public Mock<IExternalDataSource> _externalDataSource;

        [TestInitialize]
        public void SetupRepository()
        {
            _movieDataSource = new Mock<MovieDataSource>();
            _movieRepository = new List<MovieData>();
            _movieRepository.Add(new MovieData() { Genre = "Drama", Classification = "MA15+", MovieId = 32, ReleaseDate = 1977, Rating = 4, Title = "Annie Hall", Cast = new string[] { "Woody Allen", "Diane Keaton", "Carol Kane", "Paul Simon" } });
            _movieRepository.Add(new MovieData() { Genre = "Science Fiction", Classification = "MA15+", MovieId = 51, ReleaseDate = 1986, Rating = 4, Title = "The Fly", Cast = new string[] { "Jeff Goldblum", "Geena Davis", "John Getz", "Eric Stoltz" } });
            _movieRepository.Add(new MovieData() { Genre = "Horror", Classification = "MA15+", MovieId = 42, ReleaseDate = 1992, Rating = 2, Title = "Sleepwalkers", Cast = new string[] { "Joanna David", "Bill Douglas", "Fulton MacKay" } });
            _movieRepository.Add(new MovieData() { Genre = "Thriller", Classification = "R", MovieId = 77, ReleaseDate = 1995, Rating = 5, Title = "Se7en", Cast = new string[] { "Morgan Freeman", "Brad Pitt", "Kevin Spacey" } });
            _movieRepository.Add(new MovieData() { Genre = "Drama", Classification = "MA15+", MovieId = 36, ReleaseDate = 1995, Rating = 4, Title = "Twelve Monkeys", Cast = new string[] { "Madeline Stowe", "Bruce Willis", "Brad Pitt", "Christopher Plummer" } });
        }

        [TestMethod]
        public void MovieDataProvider_Search_CompareTitle()
        {
            _movieDataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var mov = new MovieDataProvider(_movieDataSource.Object);
            MovieData movie = new MovieData()
            {
                Title = "Fake movie"
            };

            var result = (from t in mov.GetAllData()
                          select t.Title).ToList();

            Assert.AreEqual(true, result.Count == 0);
        }

        [TestMethod]
        public void MovieDataProvider_Save_CreateMovie()
        {
            MovieData movie = new MovieData()
            {
                Title = "Fake movie",
                Genre = "Drama",
                Rating = 4,
                Classification = "R",
                ReleaseDate = 2016

            };
            _movieDataSource.Setup(m => m.Create(It.IsAny<MovieData>())).Callback((MovieData mov) =>
            {
                _movieRepository.Add(mov);
            });
            var create = new MovieDataProvider(_movieDataSource.Object);
            var movieId = create.Create(movie);
            if (movieId > 0)
            {
                _movieRepository.Add(movie);
            }
            _movieDataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var result = create.GetDataById(movie.MovieId);
            Assert.AreEqual("Fake movie", result.Title);
        }

        [TestMethod]
        public void MovieDataProvider_Save_UpdateMovie()
        {
            MovieData movie = new MovieData()
            {
                Genre = "Thriller",
                MovieId = 32,
            };
            _movieDataSource.Setup(m => m.Update(It.IsAny<MovieData>())).Verifiable();
            var create = new MovieDataProvider(_movieDataSource.Object);
            int movieId = create.Update(movie);
            Assert.AreEqual(32, movieId);
        }
        
    }
}
