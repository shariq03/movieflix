﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using MovieFlix.Core.Contracts;
using MoviesLibrary;
using Moq;
using MovieFlix.Web.Controllers;
using System.Linq;
using MovieFlix.Core.Implementation;

namespace MovieFlix.Tests
{
    [TestClass]
    public class IntegrationTests
    {
        public IMovieRepository _movieRepository;
        public IExternalDataSource _dataSource;
        public MovieDataSource _mds;

        [TestInitialize]
        public void Setup()
        {
            _mds = new MovieDataSource();
            _dataSource = new MovieDataProvider(_mds);
            _movieRepository = new MovieRespository(_dataSource);
        }
        [TestMethod]
        public void MoviesController_Search_Title_Returns_Movies()
        {
            MovieData mov = new MovieData() { Title = "Annie Hall" };
            var movieController = new MoviesController(_movieRepository);
            var result = movieController.LoadMovies(mov).Result.ToList();
            Assert.AreEqual("Annie Hall", result[0].Title);
        }

        [TestMethod]
        public void MoviesController_Search_MovieId_Returns_Movies()
        {
            MovieData mov = new MovieData() { MovieId = 32 };
            var movieController = new MoviesController(_movieRepository);
            var result = movieController.LoadMovies(mov).Result.ToList();
            Assert.AreEqual("Annie Hall", result[0].Title);
        }

        [TestMethod]
        public void MoviesController_Search_Rating_Returns_Movies()
        {
            MovieData mov = new MovieData() { Rating = 4 };
            var movieController = new MoviesController(_movieRepository);
            var result = movieController.LoadMovies(mov).Result.ToList();
            Assert.AreEqual(true, result.Count > 0);
        }
    }
}
