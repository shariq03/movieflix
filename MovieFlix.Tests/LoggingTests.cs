﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieFlix.Core.Contracts;
using Moq;

namespace MovieFlix.Tests
{
    [TestClass]
    public class LoggingTests
    {
        private Mock<ILoggingService> _mlogger;
        private ILoggingService _logger;
        [TestInitialize]
        public void TestSetup()
        {
            _mlogger = new Mock<ILoggingService>();
            // _logger = new LoggingService();
        }
        [TestMethod]
        public void LoggingService_Info_Log()
        {
            _mlogger.Setup(m => m.Info(It.IsAny<string>())).Verifiable();
            _logger = _mlogger.Object;
            _logger.Info("Info");
            _mlogger.VerifyAll();
        }

        [TestMethod]
        public void LoggingService_Trace_Log()
        {
            _mlogger.Setup(m => m.Trace(It.IsAny<string>())).Verifiable();
            _logger = _mlogger.Object;
            _logger.Trace("Trace");
            _mlogger.VerifyAll();
        }

        [TestMethod]
        public void LoggingService_Warn_Log()
        {
            _mlogger.Setup(m => m.Warn(It.IsAny<string>())).Verifiable();
            _logger = _mlogger.Object;
            _logger.Warn("Warn");
            _mlogger.VerifyAll();
        }

        [TestMethod]
        public void LoggingService_Error_Log()
        {
            _mlogger.Setup(m => m.Error(It.IsAny<string>())).Verifiable();
            _logger = _mlogger.Object;
            _logger.Error("Error");
            _mlogger.VerifyAll();
        }

        [TestMethod]
        public void LoggingService_Fatal_Log()
        {
            _mlogger.Setup(m => m.Fatal(It.IsAny<string>())).Verifiable();
            _logger = _mlogger.Object;
            _logger.Fatal("Fatal");
            _mlogger.VerifyAll();
        }
    }
}
