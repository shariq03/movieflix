﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieFlix.Core.Contracts;
using System.Collections.Generic;
using MoviesLibrary;
using Moq;
using MovieFlix.Core.Implementation;
using System.Linq;
using System;

namespace MovieFlix.Tests
{
    [TestClass]
    public class MovieRepositoryTests
    {
        public List<MovieData> _movieRepository;
        public Mock<IExternalDataSource> _dataSource;


        [TestInitialize]
        public void SetupRepository()
        {
            _dataSource = new Mock<IExternalDataSource>();
            _movieRepository = new List<MovieData>();
            _movieRepository.Add(new MovieData() { Genre = "Drama", Classification = "MA15+", MovieId = 32, ReleaseDate = 1977, Rating = 4, Title = "Annie Hall", Cast = new string[] { "Woody Allen", "Diane Keaton", "Carol Kane", "Paul Simon" } });
            _movieRepository.Add(new MovieData() { Genre = "Science Fiction", Classification = "MA15+", MovieId = 51, ReleaseDate = 1986, Rating = 4, Title = "The Fly", Cast = new string[] { "Jeff Goldblum", "Geena Davis", "John Getz", "Eric Stoltz" } });
            _movieRepository.Add(new MovieData() { Genre = "Horror", Classification = "MA15+", MovieId = 42, ReleaseDate = 1992, Rating = 2, Title = "Sleepwalkers", Cast = new string[] { "Joanna David", "Bill Douglas", "Fulton MacKay" } });
            _movieRepository.Add(new MovieData() { Genre = "Thriller", Classification = "R", MovieId = 77, ReleaseDate = 1995, Rating = 5, Title = "Se7en", Cast = new string[] { "Morgan Freeman", "Brad Pitt", "Kevin Spacey" } });
            _movieRepository.Add(new MovieData() { Genre = "Drama", Classification = "MA15+", MovieId = 36, ReleaseDate = 1995, Rating = 4, Title = "Twelve Monkeys", Cast = new string[] { "Madeline Stowe", "Bruce Willis", "Brad Pitt", "Christopher Plummer" } });
        }

        [TestMethod]
        public void MovieRepository_GetMovies_Returns_MoreThanZeroResult()
        {
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var ser = new MovieRespository(_dataSource.Object);
            var result = ser.LoadMovies();
            Assert.AreEqual(true, result.Count() > 0);
        }

        [TestMethod]
        public void MovieRepository_Search_CompareTitle()
        {
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var mov = new MovieRespository(_dataSource.Object);
            MovieData movie = new MovieData()
            {
                Title = "Annie Hall"
            };
            var result = (from t in mov.LoadMovies(movie)
                          select t.Title).ToList();
            Assert.AreEqual(true, result.Count > 0);
            Assert.AreEqual(true, result.Contains("Annie Hall"));
        }

        [TestMethod]
        public void MovieRepository_Search_CompareClassification()
        {
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var mov = new MovieRespository(_dataSource.Object);
            MovieData movie = new MovieData()
            {
                Classification = "MA15+"
            };
            var result = (from t in mov.LoadMovies(movie)
                          select t.Classification).ToList();
            Assert.AreEqual(true, result.Count > 0);
            Assert.AreEqual(true, result.Contains("MA15+"));
        }

        [TestMethod]
        public void MovieRepository_Search_CompareMovieId()
        {
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var mov = new MovieRespository(_dataSource.Object);
            MovieData movie = new MovieData()
            {
                MovieId = 32
            };
            var result = (from t in mov.LoadMovies(movie)
                          select t.MovieId).ToList();

            Assert.AreEqual(true, result.Count > 0);
            Assert.AreEqual(true, result.Contains(32));
        }

        [TestMethod]
        public void MovieRepository_Search_CompareCast()
        {
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var mov = new MovieRespository(_dataSource.Object);
            string[] cast = { "Brad Pitt" };
            MovieData movie = new MovieData()
            {
                Cast = new string[] { "Brad Pitt" }
            };
            var result = (from t in mov.LoadMovies(movie)
                          select t.Cast).ToList();

            Assert.AreEqual(true, result.Count == 2);
        }

        [TestMethod]
        public void MovieRepository_Search_CompareGenere()
        {
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var mov = new MovieRespository(_dataSource.Object);

            MovieData movie = new MovieData()
            {
                Genre = "Drama"
            };
            var result = (from t in mov.LoadMovies(movie)
                          select t.Genre).ToList();

            Assert.AreEqual(true, result.Count == 2);
        }

        [TestMethod]
        public void MovieRepository_Search_CompareReleaseDate()
        {
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var mov = new MovieRespository(_dataSource.Object);

            MovieData movie = new MovieData()
            {
                ReleaseDate = 1992
            };
            var result = (from t in mov.LoadMovies(movie)
                          select t.ReleaseDate).ToList();

            Assert.AreEqual(true, result.Count == 1);
        }

        [TestMethod]
        public void MovieRepository_Search_CompareRating()
        {
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var mov = new MovieRespository(_dataSource.Object);

            MovieData movie = new MovieData()
            {
                Rating = 4
            };
            var result = (from t in mov.LoadMovies(movie)
                          select t.ReleaseDate).ToList();

            Assert.AreEqual(true, result.Count == 3);
        }

        [TestMethod]
        public void MovieRepository_Search_CompareTitleReturnEmpty()
        {
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);
            var mov = new MovieRespository(_dataSource.Object);
            MovieData movie = new MovieData()
            {
                Title = "Fake movie"
            };

            var result = (from t in mov.LoadMovies(movie)
                          select t.Title).ToList();

            Assert.AreEqual(true, result.Count == 0);
        }

        [TestMethod]
        public void MovieRepository_Save_CreateMovie()
        {
            MovieData movie = new MovieData()
            {
                Title = "Fake movie",
                Genre = "Drama",
                Rating = 4,
                Classification = "R",
                ReleaseDate = 2016

            };
            _dataSource.Setup(m => m.Create(It.IsAny<MovieData>())).Callback((MovieData mov) =>
            {
                _movieRepository.Add(mov);
            });
            var create = new MovieRespository(_dataSource.Object);
            var movieId = create.SaveMovie(movie);
            if (movieId > 0)
            {
                _movieRepository.Add(movie);
            }
            _dataSource.Setup(m => m.GetAllData()).Returns(_movieRepository);

            var result = (from t in create.LoadMovies(movie)
                          select t.Title).ToList();

            Assert.AreEqual("Fake movie", result[0]);
        }

        [TestMethod]
        public void MovieRepository_Save_UpdateMovie()
        {
            MovieData movie = new MovieData()
            {
                Genre = "Thriller",
                MovieId = 32,
            };
            _dataSource.Setup(m => m.Update(It.IsAny<MovieData>())).Verifiable();
            var create = new MovieRespository(_dataSource.Object);
            int movieId = create.SaveMovie(movie);

            Assert.AreEqual(true, movieId > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MovieRepository_Save_EmptyMovieObject()
        {
            _dataSource.Setup(m => m.Update(It.IsAny<MovieData>())).Verifiable();
            var create = new MovieRespository(_dataSource.Object);
            int movieId = create.SaveMovie(null);
        }
    }

}
