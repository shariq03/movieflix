using Microsoft.Practices.Unity;
using MovieFlix.Core.Contracts;
using MovieFlix.Core.Implementation;
using MoviesLibrary;
using System.Web.Http;
using Unity.WebApi;

namespace MovieFlix.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            // register all your components with the container here
            container.RegisterType<IMovieRepository, MovieRespository>();
            container.RegisterInstance(new MovieDataSource());
            container.RegisterType<IExternalDataSource, MovieDataProvider>();
            container.RegisterType<ILoggingService, LoggingService>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}