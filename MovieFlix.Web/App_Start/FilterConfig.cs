﻿using MovieFlix.Core.Contracts;
using MovieFlix.Web.Filters;
using System.Web;
using System.Web.Mvc;

namespace MovieFlix.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
