﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace MovieFlix.Web.Filters
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;

            var exType = context.Exception.GetType();

            if (exType == typeof(UnauthorizedAccessException))
                status = HttpStatusCode.Unauthorized;
            else if (exType == typeof(ArgumentException))
                status = HttpStatusCode.NotFound;

            //log exceptions here
            //Logging.Fatal("");

            // create a new response and attach Error object

            var errorResponse =
               context.Request.CreateResponse(status, "An error occured while submitting api request");
            context.Response = errorResponse;

            base.OnException(context);
        }
    }
}