﻿using MovieFlix.Core.Contracts;
using MoviesLibrary;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using MovieFlix.Web.Filters;

namespace MovieFlix.Web.Controllers
{
    [UnhandledExceptionFilter]
    public class MoviesController : ApiController
    {
        private readonly IMovieRepository _mds;
        public MoviesController(IMovieRepository mds)
        {
            _mds = mds;
        }

        [HttpPost]
        public async Task<IEnumerable<MovieData>> LoadMovies([FromBody] MovieData movie)
        {
            var result = await Task.Run(() => _mds.LoadMovies(movie));
            return result;
        }

        [HttpPost]
        public HttpResponseMessage Save([FromBody] MovieData movie)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(
                new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    ReasonPhrase = "Validation failed."
                });
            }
            else
            {
                _mds.SaveMovie(movie);
                return this.Request.CreateResponse(HttpStatusCode.OK);
            }
        }
    }
}
